﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PANAGroupJobWebApp.Models;

namespace PANAGroupJobWebApp.Controllers
{
    public class ArithController : Controller
    {
        //
        // GET: /Arith/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult Index(int? m, int? n)
        {
            var model = new ArithModel
            {
                Sum = m + n,
                Difference = m - n,
                Multiplication = m * n,
                Ratio = n == 0 ? "?" : (m / n).ToString(),
                Remain = n == 0 ? "?" : (m % n).ToString(),
                M = m,
                N = n
            };

            return View(model);
        }

    }
}
