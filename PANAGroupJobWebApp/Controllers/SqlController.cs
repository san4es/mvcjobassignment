﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PANAGroupJobWebApp.Models.ViewModels;
using System.Linq.Dynamic;


namespace PANAGroupJobWebApp.Controllers
{
    public class SqlController : Controller
    {
        //
        // GET: /Sql/

        public ActionResult WebGrid(int page = 1, int rowsPerPage = 10, string sort = "ProductID", string sortDir = "ASC")
        {
            var model = GetProducts(page, rowsPerPage, sort, sortDir);

            return View(model);
        }

        private WebGridViewModel GetProducts(int page, int rowsPerPage, string sort, string sortDir)
        {
            using (Northwind context = new Northwind(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\northwnd.mdf;Integrated Security=True;Connect Timeout=30"))
            {
                var query = context.Products.Select(p => new
                {
                    p.ProductID,
                    p.ProductName,
                    p.Category.CategoryName,
                    p.Supplier.CompanyName,
                    p.Supplier.Country
                }).OrderBy(sort + " " + sortDir);

                WebGridViewModel model = new WebGridViewModel
                {
                    TotalRows = query.Count(),
                    Products = query.Skip((page - 1) * rowsPerPage).Take(rowsPerPage).ToList().Select(p =>
                       new HtmlViewModel
                       {
                           ProductId = p.ProductID,
                           ProductName = p.ProductName,
                           CategoryName = p.CategoryName,
                           CompanyName = p.CompanyName,
                           Country = p.Country
                       })
                };
                return model;
            }
        }

    }
}
