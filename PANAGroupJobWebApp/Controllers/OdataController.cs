﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using PANAGroupJobWebApp.Models.ViewModels;

namespace PANAGroupJobWebApp.Controllers
{
    public class OdataController : Controller
    {
        private ODataNorthwind.NorthwindEntities context = new ODataNorthwind.NorthwindEntities(new Uri("http://services.odata.org/Northwind/Northwind.svc"));
        //
        // GET: /Odata/

        public ActionResult WebGrid(int page = 1, int rowsPerPage = 10, string sort = "ProductID", string sortDir = "ASC")
        {
            var data = GetProducts(page, rowsPerPage, sort, sortDir);

            return View(data);
        }

        private string DefineSortColumn(string sort)
        {
            switch (sort)
            {
                case "CategoryName":
                    return "Category/" + sort;
                case "CompanyName":
                    return "Supplier/" + sort;
                case "Country":
                    return "Supplier/" + sort;
                default:
                    return sort;
            }
        }

        // Data for WebGridObj
        private WebGridViewModel GetProducts(int page, int rowsPerPage, string sort, string sortDir)
        {
            string orderBy = DefineSortColumn(sort);
            var query = context.Products.AddQueryOption("$orderby", orderBy + " " + sortDir.ToLower());
            var q1 = query.Select(p => new
                {
                    p.ProductID,
                    p.ProductName,
                    p.Category.CategoryName,
                    p.Supplier.CompanyName,
                    p.Supplier.Country
                });

            WebGridViewModel model = new WebGridViewModel
            {
                TotalRows = q1.Count(),
                Products = q1.Skip((page - 1) * rowsPerPage).Take(rowsPerPage).ToList().Select(p =>
                   new HtmlViewModel
                   {
                       ProductId = p.ProductID,
                       ProductName = p.ProductName,
                       CategoryName = p.CategoryName,
                       CompanyName = p.CompanyName,
                       Country = p.Country
                   })
            };
            return model;
        }

    }
}
