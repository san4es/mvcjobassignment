﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using PANAGroupJobWebApp.DataLayer;
using PANAGroupJobWebApp.Models.ViewModels;

namespace PANAGroupJobWebApp.Controllers
{
    public class ObjController : Controller
    {
        //
        // GET: /HtmlObj/
        private NorthwindEntities context = new NorthwindEntities();

        public ActionResult Html()
        {
            var data = context.Products.Select(p =>
                new HtmlViewModel
                {
                    ProductId = p.ProductID,
                    ProductName = p.ProductName,
                    CategoryName = p.Categories.CategoryName,
                    CompanyName = p.Suppliers.CompanyName,
                    Country = p.Suppliers.Country
                });

            return View(data);
        }

        // GET: /WebGrid/
        public ActionResult WebGrid(int page = 1, int rowsPerPage = 10, string sort = "ProductID", string sortDir = "ASC")
        {
            var data = GetProducts(page, rowsPerPage, sort, sortDir);
            return View(data);
        }



        // Data for WebGridObj
        private WebGridViewModel GetProducts(int page, int rowsPerPage, string sort, string sortDir)
        {
            var query = context.Products.Select(p => new 
            {
                p.ProductID,
                p.ProductName,
                p.Categories.CategoryName,
                p.Suppliers.CompanyName,
                p.Suppliers.Country
            }).OrderBy(sort + " " + sortDir);

            WebGridViewModel model = new WebGridViewModel
            {
                TotalRows = query.Count(),
                Products = query.Skip((page - 1) * rowsPerPage).Take(rowsPerPage).ToList().Select(p =>
                   new HtmlViewModel
                   {
                       ProductId = p.ProductID,
                       ProductName = p.ProductName,
                       CategoryName = p.CategoryName,
                       CompanyName = p.CompanyName,
                       Country = p.Country
                   })
            };
            return model;
        }

    }
}
