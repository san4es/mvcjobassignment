﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PANAGroupJobWebApp.Models.ViewModels;
using System.Linq.Dynamic;
using System.Xml.Linq;

namespace PANAGroupJobWebApp.Controllers
{
    public class XmlController : Controller
    {
        //
        // GET: /Xml/

        public ActionResult WebGrid(int page = 1, int rowsPerPage = 10, string sort = "ProductID", string sortDir = "ASC")
        {
            var model = GetProducts(page, rowsPerPage, sort, sortDir);
            return View(model);
        }

        private WebGridViewModel GetProducts(int page, int rowsPerPage, string sort, string sortDir)
        {
            string path = System.Web.HttpContext.Current.Server.MapPath(@"~\App_Data");
            var Products = XElement.Load(path + @"\XProducts.xml");
            var Categories = XElement.Load(path + @"\XCategories.xml");
            var Suppliers = XElement.Load(path + @"\XSuppliers.xml");

            var query = Products.Descendants("Product")
                .Join(Categories.Descendants("Category"), product => product.Element("CategoryID").Value, category => category.Element("CategoryID").Value,
                    (product, category) =>
                        new
                        {
                            ProductID = product.Element("ProductID").Value,
                            ProductName = product.Element("ProductName").Value,
                            CategoryName = category.Element("CategoryName").Value,
                            SupplierID = product.Element("SupplierID").Value
                        })
                .Join(Suppliers.Descendants("Supplier"), product => product.SupplierID, supplier => supplier.Element("SupplierID").Value,
                    (product, supplier) =>
                        new HtmlViewModel
                        {
                            ProductId = Int32.Parse(product.ProductID),
                            ProductName = product.ProductName,
                            CategoryName = product.CategoryName,
                            CompanyName = supplier.Element("CompanyName").Value,
                            Country = supplier.Element("Country").Value
                        })
                .AsQueryable<HtmlViewModel>().OrderBy(sort + " " + sortDir);

            WebGridViewModel model = new WebGridViewModel
            {
                TotalRows = query.Count(),
                Products = query.Skip((page - 1) * rowsPerPage).Take(rowsPerPage).ToList()
            };
            return model;
        }

    }
}
