
0. This file

* _Readme.txt

-------

1. Data

* Obj data code: lists included in northwind_obj_join.linq, to be copied in your application.

* SQL data file: northwnd.mdf, to be copied to c:\usrtemp\northwnd.mdf.

* XML data files: XCategories.xml, XProducts.xml, XSuppliers.xml.

* Odata data: accessed by URL, http://services.odata.org/Northwind/Northwind.svc/.

-------

2. Sample snippets related to controller codes

* northwind_obj_join.linq
  Ideas for the Obj controller, using joins.
  This snippet contains data lists code to be copied in your application.

* northwind_sql_join.linq
* northwind_sql_nav.linq
  Ideas for the Sql controller, using either joins or navigation.
  The database file is expected on c:\usrtemp\northwnd.mdf.

* northwind_xml_join.linq
  Ideas for the Xml controller, using joins.
  The Xml files are expected in the same folder: XCategories.xml, XProducts.xml, XSuppliers.xml.

* northwind_odata_nav.linq
  Ideas for the Odata controller, using navigation expressed in LINQ or directly in URLs.
  Basic part only, no bonus.

-------

2. Generation snippets
  
* northwind_to_xml.linq
  Reads the Sql database, expected on c:\usrtemp\northwnd.mdf, 
  (re)generates the Xml files: XCategories.xml, XProducts.xml, XSuppliers.xml, and
  (re)generates the Obj code, by printing it to the console output  

* _DataSvcUtil NorthwindOdata.bat
* NorthwindOdata.cs
* NorthwindOdata.dll
  This batch launches DataSvcUtil.exe, which reads the Odata service and
  (re)generates and compiles a data service context class, 
  NorthwindOdata.cs/.dll, that could (but need not) be used for Odata.
  Note that Linqpad and VS have wizards that generate similar classes,
  so this sample is primarly given for study and completeness.
    
* _SqlMetal NorthwindSQL.bat
* NorthwindSql.cs
* NorthwindSql.dll
  This batch launches SqlMetal.exe, which reads the Sql database and 
  (re)generates and compiles a data context class,
  NorthwindSql.cs/.dll, that could (but need not) be used for SQL.
  Note that Linqpad and VS have wizards that generate similar classes,
  so this sample is primarily given for study and completeness.

-------

3. Miscellanea

* dynamic-sort.linq
  Dynamic sorting samples without Dynamic LINQ.
  
* dynamic-sort2.linq
  Dynamic LINQ samples, including sorting.

* Dynamic.cs
* Dynamic.dll
* _Compile Dynamic.bat
  Dynamic LINQ: source, library, compilation batch.

-------

