﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PANAGroupJobWebApp.Models
{
    public class ArithModel
    {
        [Display(Name = "M = ")]
        public int? M { get; set; }

        [Display(Name = "N = ")]
        public int? N { get; set; }

        [Display(Name = "M + N = ")]
        public int? Sum { get; set; }

        [Display(Name = "M - N = ")]
        public int? Difference { get; set; }

        [Display(Name = "M * N = ")]
        public int? Multiplication { get; set; }

        [Display(Name = "M / N = ")]
        public string Ratio { get; set; }

        [Display(Name = "M % N = ")]
        public string Remain { get; set; }

    }
}