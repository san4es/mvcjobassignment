﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PANAGroupJobWebApp.Models.ViewModels
{
    public class HtmlViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
    }
}