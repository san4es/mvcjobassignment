﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PANAGroupJobWebApp.Models.ViewModels
{
    public class WebGridViewModel
    {
        public int TotalRows { get; set; }
        public IEnumerable<HtmlViewModel> Products { get; set; }
    }
}